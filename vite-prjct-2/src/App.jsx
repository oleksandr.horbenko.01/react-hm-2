import './App.css';
import ProductList from './ProductList';
import { useState, useEffect } from 'react';
import CartPage from './CartPage';
import FavoritesPage from './FavoritesPage';
import { BrowserRouter as Router, Route, Link, Routes } from 'react-router-dom';

function App() {
    const [products, setProducts] = useState([]);
    const [cart, setCart] = useState([]);
    const [favorites, setFavorites] = useState([]);
  
    useEffect(() => {
      fetchProducts();
      loadCartFromLocalStorage();
      loadFavoritesFromLocalStorage();
    }, []);
  
    const fetchProducts = async () => {
      try {
        const response = await fetch('/products.json');
        const data = await response.json();
        setProducts(data.data);
      } catch (error) {
        console.error('Error fetching products:', error);
      }
    };
  
    const addToCart = productId => {
      const productToAdd = products.find(product => product.id === productId);
      setCart([...cart, productToAdd]);
      saveCartToLocalStorage([...cart, productToAdd]);
    };
  
    const addToFavorites = productId => {
      const productToAdd = products.find(product => product.id === productId);
      setFavorites([...favorites, productToAdd]);
      saveFavoritesToLocalStorage([...favorites, productToAdd]);
    };

    const removeFromCart = productId => {
      const updatedCart = cart.filter(product => product.id !== productId);
      setCart(updatedCart);
      saveCartToLocalStorage(updatedCart);
    };

    const removeFromFavorites = productId => {
      const updatedFavorites = favorites.filter(product => product.id !== productId);
      setFavorites(updatedFavorites);
      saveFavoritesToLocalStorage(updatedFavorites);
    };
  
    const saveCartToLocalStorage = cartData => {
      localStorage.setItem('cart', JSON.stringify(cartData));
    };
  
    const saveFavoritesToLocalStorage = favoritesData => {
      localStorage.setItem('favorites', JSON.stringify(favoritesData));
    };
  
    const loadCartFromLocalStorage = () => {
      const savedCart = JSON.parse(localStorage.getItem('cart')) || [];
      setCart(savedCart);
    };
  
    const loadFavoritesFromLocalStorage = () => {
      const savedFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
      setFavorites(savedFavorites);
    };
  
    return (
      <Router>
        <div className="App">
         <header>
            <h1>Online Store</h1>
            <div id="cart-icon">🛒 <span>{cart.length}</span></div>
            <div id="favorites-icon">⭐️ <span>{favorites.length}</span></div>
            <nav>
              <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/cart">Cart</Link></li>
                <li><Link to="/favorites">Favorites</Link></li>
              </ul>
            </nav>
          </header>
          <main>
          <Routes>
            <Route exact path="/" element={<ProductList 
             products={products} 
             addToCart={addToCart} 
             addToFavorites={addToFavorites} />}
            />
            <Route path="/cart" element={<CartPage
             cart={cart}
             removeFromCart={removeFromCart} />} 
            />
            <Route path="/favorites" element={<FavoritesPage
             favorites={favorites}
             removeFromFavorites={removeFromFavorites} />} 
            />
          </Routes>
          </main>        
        </div>
      </Router>
    );
  }
  
  export default App;