import React from 'react';
import ProductCard from './ProductCard';

function FavoritesPage({ favorites, removeFromFavorites }) {
  return (
    <div className="favorites-page">
      <h2>Вибране</h2>
      {favorites.map(product => (
        <div key={product.id}>
          <ProductCard product={product} />
          <button onClick={() => removeFromFavorites(product.id)}>X</button>
        </div>
      ))}
    </div>
  );
}

export default FavoritesPage;