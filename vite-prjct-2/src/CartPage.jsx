import React from 'react';
import { useState } from 'react';
import ProductCard from './ProductCard';


function CartPage({ cart, removeFromCart }) {

  const [showConfirmation, setShowConfirmation] = useState(false);
  const [productToRemove, setProductToRemove] = useState(null);

  const handleRemoveFromCart = (productId) => {
    setProductToRemove(productId);
    setShowConfirmation(true);
  };

  const confirmRemoveFromCart = () => {
    removeFromCart(productToRemove);
    setShowConfirmation(false);
  };

  const cancelRemoveFromCart = () => {
    setProductToRemove(null);
    setShowConfirmation(false);
  };

  return (
    <div className="cart-page">
      <h2>Кошик</h2>
      {cart.map(product => (
        <div key={product.id}>
          <ProductCard product={product} />
          <button onClick={() => handleRemoveFromCart(product.id)}>X</button>
        </div>
      ))}
      {showConfirmation && (
        <div className="modal">
          <div className="modal-content">
            <p>Ви впевнені, що хочете видалити цей товар з кошика?</p>
            <button onClick={confirmRemoveFromCart}>Так</button>
            <button onClick={cancelRemoveFromCart}>Ні</button>
          </div>
        </div>
      )}
    </div>
  );
}

export default CartPage;