import React from 'react';
import ProductCard from './ProductCard';
import PropTypes from 'prop-types';

function ProductList({ products, addToCart, addToFavorites }) {
  return (
    <div className="product-list">
      {products.map(product => (
        <ProductCard
          key={product.id}
          product={product}
          addToCart={addToCart}
          addToFavorites={addToFavorites}
        />
      ))}
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.array,
  addToCart: PropTypes.func,
  addToFavorites: PropTypes.func
};

ProductList.defaultProps = {
  products: [],
  addToCart: () => {},
  addToFavorites: () => {}
};

export default ProductList;