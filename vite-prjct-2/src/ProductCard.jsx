import React, { useState } from 'react';
import './ProductCard.css';
import PropTypes from 'prop-types';

function ProductCard({ product, addToCart, addToFavorites }) {

  const [openModal, setOpenModal] = useState(false);
  const [addedToFavorites, setAddedToFavorites] = useState(false);


  const handleAddToCart = () => {
    addToCart(product.id);
    setOpenModal(true);
  };

  const handleAddToFavorites = () => {
    addToFavorites(product.id);
    setAddedToFavorites(true);
  };

  const closeModal = () => {
    setOpenModal(false);
  };

  return (
    <div className="product-card">
      <img src={product.image} alt={product.failure} className='img' />
      <h2 style={{ color: product.color }}>{product.name}</h2>
      <p>Price: {product.price}$</p>
      <button onClick={handleAddToCart}>Add to Cart</button>
      <button onClick={handleAddToFavorites} style={{ color: addedToFavorites ? 'gold' : 'black' }}>★</button>

      {openModal && (
        <div className="modal">
          <div className="modal-content">
            <p>Product added to cart!</p>
            <button onClick={closeModal}>Close</button>
          </div>
        </div>
      )}
    </div>
  );
}

ProductCard.propTypes = {
  product: PropTypes.object,
  addToCart: PropTypes.func,
  addToFavorites: PropTypes.func
};

ProductCard.defaultProps = {
  product: {},
  addToCart: () => {},
  addToFavorites: () => {}
};

export default ProductCard;